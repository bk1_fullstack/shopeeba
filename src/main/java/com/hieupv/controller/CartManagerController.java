package com.hieupv.controller;

import com.hieupv.common.apiResult.ApiResults;
import com.hieupv.dto.api.param.UpdateCartManagerParamDTO;
import com.hieupv.dto.api.results.UpdateCartManagerDTO;
import com.hieupv.dto.api.results.ViewCartManagerDTO;
import com.hieupv.dto.api.results.ViewListCartManagerDTO;
import com.hieupv.entity.CartManagement;
import com.hieupv.service.CartManagerServices;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Cart Manager Controller
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 18-08-2023
 */

@RestController
@RequestMapping("api/cart-manager")
@RequiredArgsConstructor
public class CartManagerController {
    private final CartManagerServices cartManagerServices;

    /**
     * Get All Cart Manager
     *
     * @return
     * @throws Exception
     */
    @GetMapping("list")
    public ResponseEntity<ApiResults> getAllCartManager() throws Exception {
        // Get All User
        List<CartManagement> dbLists = cartManagerServices.listAllCartManagerData();
        List<ViewListCartManagerDTO> viewListCartManagerDTOS = new ArrayList<>();

        for (CartManagement cartManagement : dbLists
        ) {
            ViewListCartManagerDTO viewListCartManagerDTO = new ViewListCartManagerDTO();

            viewListCartManagerDTO.setId(cartManagement.getId());
            viewListCartManagerDTO.setTotalPrice(cartManagement.getTotalPrice());
            viewListCartManagerDTO.setTotalCount(cartManagement.getTotalCount());
            viewListCartManagerDTO.setProductManagements(cartManagement.getProductManagements());
            viewListCartManagerDTO.setAccountInfoManager(cartManagement.getAccountInfoManager());
            viewListCartManagerDTO.setQuantityProduct(cartManagement.getQuantityProduct());

            viewListCartManagerDTOS.add(viewListCartManagerDTO);
        }
        var apiResult = new ApiResults(true);
        apiResult.setData(viewListCartManagerDTOS);

        return ResponseEntity.ok(apiResult);
    }

    /**
     * @param cartManagerId
     * @return
     * @throws Exception
     */
    @GetMapping("/detail/{cartManagerId}")
    public ResponseEntity<ApiResults> getCartManagerById(@PathVariable("cartManagerId") int cartManagerId) throws Exception {
        // Get All User
       CartManagement result = cartManagerServices.getCartManagerDataById(cartManagerId);
        ViewCartManagerDTO viewCartManagerDTO = new ViewCartManagerDTO();

        viewCartManagerDTO.setId(result.getId());
        viewCartManagerDTO.setTotalPrice(result.getTotalPrice());
        viewCartManagerDTO.setTotalCount(result.getTotalCount());
        viewCartManagerDTO.setProductManagements(result.getProductManagements());
        viewCartManagerDTO.setAccountInfoManager(result.getAccountInfoManager());
        viewCartManagerDTO.setQuantityProduct(result.getQuantityProduct());

        var apiResult = new ApiResults(true);
        apiResult.setData(viewCartManagerDTO);
        return ResponseEntity.ok(apiResult);
    }

    /**
     * Modify Cart Manager Data
     *
     * @param updateCartManagerParamDTO
     * @return
     * @throws Exception
     */
    @PutMapping
    public ResponseEntity<ApiResults> modifyCartManagerData(@RequestBody @Validated UpdateCartManagerParamDTO updateCartManagerParamDTO) throws Exception {
        var updateCartManagerDTO = new UpdateCartManagerDTO();

        updateCartManagerDTO.setId(updateCartManagerParamDTO.getId());
        updateCartManagerDTO.setProductManagements(updateCartManagerParamDTO.getProductManagements());
        updateCartManagerDTO.setQuantityProduct(updateCartManagerParamDTO.getQuantityProduct());

        this.cartManagerServices.modifyCartManagerData(updateCartManagerDTO);

        var apiResult = new ApiResults<>(true);

        apiResult.setData(updateCartManagerDTO);

        return ResponseEntity.ok(apiResult);
    }

    /**
     * Remove Cart Manager Data
     *
     * @param id
     * @return
     * @throws Exception
     */
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<ApiResults> removeCategoryManagerData(@PathVariable("id") Integer id) throws Exception {
        return this.cartManagerServices.removeCartManagerDataById(id);
    }
}
