package com.hieupv.controller;

import com.hieupv.common.apiResult.ApiResults;
import com.hieupv.dto.api.param.CreateProductManagerParamDTO;
import com.hieupv.dto.api.param.UpdateProductManagerParamDTO;
import com.hieupv.dto.api.results.CreateProductManagerDTO;
import com.hieupv.dto.api.results.UpdateProductManagerDTO;
import com.hieupv.dto.api.results.ViewListProductManagerDTO;
import com.hieupv.dto.api.results.ViewProductManagerDTO;
import com.hieupv.entity.ProductManagement;
import com.hieupv.service.ProductManagerServices;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Product Manager Controller
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 15-08-2023
 */

@RestController
@RequestMapping("api/product-manager")
@RequiredArgsConstructor
public class ProductManagerController {
    private final ProductManagerServices productManagerServices;

    /**
     * Get All Product Manager
     *
     * @return
     * @throws Exception
     */
    @GetMapping("list")
    public ResponseEntity<ApiResults> getAllProductManager() throws Exception {
        // Get All Product
        List<ProductManagement> dbLists = productManagerServices.listAllProductManagerData();
        List<ViewListProductManagerDTO> viewListProductManagerDTOS = new ArrayList<>();

        for (ProductManagement productManagement : dbLists
        ) {
            ViewListProductManagerDTO viewListProductManagerDTO = new ViewListProductManagerDTO();

            viewListProductManagerDTO.setProductName(productManagement.getProductName());
            viewListProductManagerDTO.setDescription(productManagement.getDescription());
            viewListProductManagerDTO.setPrice(productManagement.getPrice());
            viewListProductManagerDTO.setStock(productManagement.getStock());
            viewListProductManagerDTO.setId(productManagement.getId());
            viewListProductManagerDTO.setCreateDate(productManagement.getCreateDate());
            viewListProductManagerDTO.setQuantitySold(productManagement.getQuantitySold());
            viewListProductManagerDTO.setModifyDate(productManagement.getModifyDate());
            viewListProductManagerDTO.setModifyByUserId(productManagement.getModifyByUserId());

            viewListProductManagerDTOS.add(viewListProductManagerDTO);
        }
        var apiResult = new ApiResults(true);
        apiResult.setData(viewListProductManagerDTOS);

        return ResponseEntity.ok(apiResult);
    }

    /**
     * Get Product Manager By Id
     * @param productManagerId
     * @return
     * @throws Exception
     */
    @GetMapping("/detail/{productManagerId}")
    public ResponseEntity<ApiResults> getProductManagerById(@PathVariable("productManagerId") int productManagerId) throws Exception {
        // Get All User
        ProductManagement dbResults = productManagerServices.getProductManagerDataById(productManagerId);
        ViewProductManagerDTO viewProductManagerDTO = new ViewProductManagerDTO();

        viewProductManagerDTO.setProductName(dbResults.getProductName());
        viewProductManagerDTO.setDescription(dbResults.getDescription());
        viewProductManagerDTO.setPrice(dbResults.getPrice());
        viewProductManagerDTO.setStock(dbResults.getStock());
        viewProductManagerDTO.setId(dbResults.getId());
        viewProductManagerDTO.setCreateDate(dbResults.getCreateDate());
        viewProductManagerDTO.setQuantitySold(dbResults.getQuantitySold());
        viewProductManagerDTO.setModifyDate(dbResults.getModifyDate());
        viewProductManagerDTO.setModifyByUserId(dbResults.getModifyByUserId());

        var apiResult = new ApiResults(true);
        apiResult.setData(viewProductManagerDTO);
        return ResponseEntity.ok(apiResult);
    }

    /**
     * Create new Product Manager
     *
     * @param createProductManagerParamDTO
     * @return
     * @throws Exception
     */
    @PostMapping
    public ResponseEntity<ApiResults> createProductManagerData(@RequestBody @Validated CreateProductManagerParamDTO createProductManagerParamDTO) throws Exception {
        // Set createProductManagerParamDTO information to createProductManagerDTO
        var createProductManagerDTO = new CreateProductManagerDTO();
        createProductManagerDTO.setProductName(createProductManagerParamDTO.getProductName());
        createProductManagerDTO.setDescription(createProductManagerParamDTO.getDescription());
        createProductManagerDTO.setPrice(createProductManagerParamDTO.getPrice());
        createProductManagerDTO.setStock(createProductManagerParamDTO.getStock());
        createProductManagerDTO.setCategoryId(createProductManagerParamDTO.getCategoryId());

        this.productManagerServices.createProductManagerData(createProductManagerDTO);

        var apiResult = new ApiResults(true);
        return ResponseEntity.ok(apiResult);
    }

    /**
     * Modify Product Manager Data
     *
     * @param updateProductManagerParamDTO
     * @return
     * @throws Exception
     */
    @PutMapping
    public ResponseEntity<ApiResults> modifyProductManagerData(@RequestBody @Validated UpdateProductManagerParamDTO updateProductManagerParamDTO) throws Exception {

        var updateProductManagerDTO = new UpdateProductManagerDTO();

            updateProductManagerDTO.setProductName(updateProductManagerParamDTO.getProductName());
            updateProductManagerDTO.setDescription(updateProductManagerParamDTO.getDescription());
            updateProductManagerDTO.setId(updateProductManagerParamDTO.getId());
            updateProductManagerDTO.setStock(updateProductManagerParamDTO.getStock());
            updateProductManagerDTO.setPrice(updateProductManagerParamDTO.getPrice());
            updateProductManagerDTO.setCategoryId(updateProductManagerParamDTO.getCategoryId());

        this.productManagerServices.modifyProductManagerData(updateProductManagerDTO);

        var apiResult = new ApiResults<>(true);
        apiResult.setData(updateProductManagerDTO);

        return ResponseEntity.ok(apiResult);
    }

    /**
     * Remove Product Manager Data
     *
     * @param id
     * @return
     * @throws Exception
     */
    @DeleteMapping(path = "/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ApiResults> removeProductManagerData(@PathVariable("id") Integer id) throws Exception {
        this.productManagerServices.removeProductManagerDataById(id);
        return ResponseEntity.ok(new ApiResults(true));
    }
}
