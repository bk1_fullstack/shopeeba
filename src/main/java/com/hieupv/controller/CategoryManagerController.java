package com.hieupv.controller;

import com.hieupv.common.apiResult.ApiResults;
import com.hieupv.dto.api.AuthenticationResponse;
import com.hieupv.dto.api.param.*;
import com.hieupv.dto.api.results.*;
import com.hieupv.entity.AccountInfoManager;
import com.hieupv.entity.CategoryManagement;
import com.hieupv.repository.CategoryManagerRepository;
import com.hieupv.service.AuthenticationService;
import com.hieupv.service.CategoryManagerServices;
import com.hieupv.service.UserInfoManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Category Manager Controller
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 08-08-2023
 */

@RestController
@RequestMapping("api/category-manager")
@RequiredArgsConstructor
public class CategoryManagerController {
    private final CategoryManagerServices categoryManagerServices;

    /**
     * Get All Category Manager
     *
     * @return
     * @throws Exception
     */
    @GetMapping("list")
    public ResponseEntity<ApiResults> getAllCategoryManager() throws Exception {
        // Get All User
        List<CategoryManagement> dbLists = categoryManagerServices.listAllCategoryManagerData();
        List<ViewListCategoryManagerDTO> viewListCategoryManagerDTOS = new ArrayList<>();

        for (CategoryManagement categoryManagement : dbLists
        ) {
            ViewListCategoryManagerDTO viewListCategoryManagerDTO = new ViewListCategoryManagerDTO();

            viewListCategoryManagerDTO.setCategoryName(categoryManagement.getCategoryName());
            viewListCategoryManagerDTO.setId(categoryManagement.getId());
            viewListCategoryManagerDTO.setCreateDate(categoryManagement.getCreateDate());
            viewListCategoryManagerDTO.setModifyDate(categoryManagement.getModifyDate());
            viewListCategoryManagerDTO.setModifyByUserId(categoryManagement.getModifyByUserId());

            viewListCategoryManagerDTOS.add(viewListCategoryManagerDTO);
        }
        var apiResult = new ApiResults(true);
        apiResult.setData(viewListCategoryManagerDTOS);

        return ResponseEntity.ok(apiResult);
    }

    /**
     *
     * @param categoryManagerId
     * @return
     * @throws Exception
     */
    @GetMapping("/detail/{categoryManagerId}")
    public ResponseEntity<ApiResults> getUserInfoManagerById(@PathVariable("categoryManagerId") int categoryManagerId) throws Exception {
        // Get All User
        CategoryManagement dbResults = categoryManagerServices.getCategoryManagerDataById(categoryManagerId);
        ViewCategoryManagerDTO viewCategoryManagerDTO = new ViewCategoryManagerDTO();

        viewCategoryManagerDTO.setCategoryName(dbResults.getCategoryName());
        viewCategoryManagerDTO.setId(dbResults.getId());
        viewCategoryManagerDTO.setCreateDate(dbResults.getCreateDate());
        viewCategoryManagerDTO.setModifyDate(dbResults.getModifyDate());
        viewCategoryManagerDTO.setModifyByUserId(dbResults.getModifyByUserId());

        var apiResult = new ApiResults(true);
        apiResult.setData(viewCategoryManagerDTO);
        return ResponseEntity.ok(apiResult);
    }

    /**
     * Create new Category Manager
     *
     * @param createCategoryManagerParamDTO
     * @return
     * @throws Exception
     */
    @PostMapping
    public ResponseEntity<ApiResults> createCategoryManagerData(@RequestBody @Validated CreateCategoryManagerParamDTO createCategoryManagerParamDTO) throws Exception {
        // Set createUserParamDTO information to createUserDTO
        var createCategoryManagerDTO = new CreateCategoryManagerDTO();
        createCategoryManagerDTO.setCategoryName(createCategoryManagerParamDTO.getCategoryName());

        this.categoryManagerServices.createCategoryManagerData(createCategoryManagerDTO);

        var apiResult = new ApiResults(true);
        return ResponseEntity.ok(apiResult);
    }

    /**
     * Modify Category Manager Data
     *
     * @param updateCategoryManagerParamDTO
     * @return
     * @throws Exception
     */
    @PutMapping
    public ResponseEntity<ApiResults> modifyCategoryManagerData(@RequestBody @Validated UpdateCategoryManagerParamDTO updateCategoryManagerParamDTO) throws Exception {

        var updateCategoryManagerDTO = new UpdateCategoryManagerDTO();
        updateCategoryManagerDTO.setCategoryName(updateCategoryManagerParamDTO.getCategoryName());
        updateCategoryManagerDTO.setId(updateCategoryManagerParamDTO.getId());

        this.categoryManagerServices.modifyCategoryManagerData(updateCategoryManagerDTO);

        var apiResult = new ApiResults<>(true);
        apiResult.setData(updateCategoryManagerParamDTO);

        return ResponseEntity.ok(apiResult);
    }

    /**
     * Remove Category Manager Data
     *
     * @param id
     * @return
     * @throws Exception
     */
    @DeleteMapping(path = "/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ApiResults> removeCategoryManagerData(@PathVariable("id") Integer id) throws Exception {
        return this.categoryManagerServices.removeCategoryManagerDataById(id);
    }
}
