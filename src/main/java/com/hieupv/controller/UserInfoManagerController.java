package com.hieupv.controller;

import com.hieupv.common.apiResult.ApiResults;
import com.hieupv.dto.api.AuthenticationResponse;
import com.hieupv.dto.api.param.AuthenticationRequestParamDTO;
import com.hieupv.dto.api.param.CreateUserInfoManagerParamDTO;
import com.hieupv.dto.api.param.UpdateUserInfoManagerParamDTO;
import com.hieupv.dto.api.results.CreateUserInfoManagerDTO;
import com.hieupv.dto.api.results.UpdateUserInfoManagerDTO;
import com.hieupv.dto.api.results.ViewListUserInfoManagerDTO;
import com.hieupv.dto.api.results.ViewUserInfoManagerDTO;
import com.hieupv.entity.AccountInfoManager;
import com.hieupv.service.AuthenticationService;
import com.hieupv.service.UserInfoManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * User Info Manager Controller
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 04-08-2023
 */

@RestController
@RequestMapping("api/user-info-manager")
@RequiredArgsConstructor
public class UserInfoManagerController {
    private final UserInfoManagerService userInfoManagerService;
    private final AuthenticationService authenticationService;
    private final PasswordEncoder passwordEncoder;

    /**
     * Get All User Info Manager
     *
     * @return
     * @throws Exception
     */
    @GetMapping("list")
    public ResponseEntity<ApiResults> getAllUserInfoManager() throws Exception {
        // Get All User
        List<AccountInfoManager> dbLists = userInfoManagerService.listAllUserInfoManagerData();
        List<ViewListUserInfoManagerDTO> viewListUserInfoManagerDTO = new ArrayList<>();

        for (AccountInfoManager listItemResults : dbLists
        ) {
            ViewListUserInfoManagerDTO viewListUserInfoManagerDTO1 = new ViewListUserInfoManagerDTO();

            viewListUserInfoManagerDTO1.setModifyByUserId(listItemResults.getModifyByUserId());
            viewListUserInfoManagerDTO1.setId(listItemResults.getId());
            viewListUserInfoManagerDTO1.setAddress(listItemResults.getAddress());
            viewListUserInfoManagerDTO1.setBirthday(listItemResults.getBirthday());
            viewListUserInfoManagerDTO1.setFirstname(listItemResults.getFirstname());
            viewListUserInfoManagerDTO1.setLastname(listItemResults.getLastname());
            viewListUserInfoManagerDTO1.setRole(listItemResults.getRole());
            viewListUserInfoManagerDTO1.setAvatarUrl(listItemResults.getAvatarUrl());
            viewListUserInfoManagerDTO1.setUsername(listItemResults.getUsername());
            viewListUserInfoManagerDTO1.setCreateDate(listItemResults.getCreateDate());
            viewListUserInfoManagerDTO1.setModifyDate(listItemResults.getModifyDate());
            viewListUserInfoManagerDTO1.setPhoneNumber(listItemResults.getPhoneNumber());

            viewListUserInfoManagerDTO.add(viewListUserInfoManagerDTO1);

        }

        var apiResult = new ApiResults(true);
        apiResult.setData(viewListUserInfoManagerDTO);
        return ResponseEntity.ok(apiResult);
    }

    /**
     * Get All User Info Manager By user Id
     *
     * @return
     * @throws Exception
     */
    @GetMapping("/detail/{userId}")
    public ResponseEntity<ApiResults> getUserInfoManagerById(@PathVariable("userId") int userId) throws Exception {
        // Get All User
        AccountInfoManager dbResults = userInfoManagerService.getUserInfoManagerDataById(userId);
        ViewUserInfoManagerDTO viewUserInfoManagerDTO = new ViewUserInfoManagerDTO();

        viewUserInfoManagerDTO.setModifyByUserId(dbResults.getModifyByUserId());
        viewUserInfoManagerDTO.setId(dbResults.getId());
        viewUserInfoManagerDTO.setAddress(dbResults.getAddress());
        viewUserInfoManagerDTO.setBirthday(dbResults.getBirthday());
        viewUserInfoManagerDTO.setFirstname(dbResults.getFirstname());
        viewUserInfoManagerDTO.setLastname(dbResults.getLastname());
        viewUserInfoManagerDTO.setRole(dbResults.getRole());
        viewUserInfoManagerDTO.setAvatarUrl(dbResults.getAvatarUrl());
        viewUserInfoManagerDTO.setUsername(dbResults.getUsername());
        viewUserInfoManagerDTO.setCreateDate(dbResults.getCreateDate());
        viewUserInfoManagerDTO.setModifyDate(dbResults.getModifyDate());
        viewUserInfoManagerDTO.setPhoneNumber(dbResults.getPhoneNumber());

        var apiResult = new ApiResults(true);
        apiResult.setData(viewUserInfoManagerDTO);
        return ResponseEntity.ok(apiResult);
    }

    /**
     * Create new User
     *
     * @param createUserInfoManagerParamDTO
     * @return
     * @throws Exception
     */
    @PostMapping
    public ResponseEntity<ApiResults> createUserInfoManagerData(@RequestBody @Validated CreateUserInfoManagerParamDTO createUserInfoManagerParamDTO) throws Exception {
        // Set createUserParamDTO information to createUserDTO
        var createUserInfoManagerDTO = new CreateUserInfoManagerDTO();

        createUserInfoManagerDTO.setUsername(createUserInfoManagerParamDTO.getUsername());
        createUserInfoManagerDTO.setPassword(passwordEncoder.encode(createUserInfoManagerParamDTO.getPassword()));
        createUserInfoManagerDTO.setFirstname(createUserInfoManagerParamDTO.getFirstname());
        createUserInfoManagerDTO.setLastname(createUserInfoManagerParamDTO.getLastname());
        createUserInfoManagerDTO.setBirthday(createUserInfoManagerParamDTO.getBirthday());
        createUserInfoManagerDTO.setAvatarUrl(createUserInfoManagerParamDTO.getAvatarUrl());
        createUserInfoManagerDTO.setPhoneNumber(createUserInfoManagerParamDTO.getPhoneNumber());
        createUserInfoManagerDTO.setAddress(createUserInfoManagerParamDTO.getAddress());
        createUserInfoManagerDTO.setRole(createUserInfoManagerParamDTO.getRole());

        this.userInfoManagerService.createUserInfoManagerData(createUserInfoManagerDTO);

        var apiResult = new ApiResults(true);
        return ResponseEntity.ok(apiResult);
    }

    /**
     * login
     *
     * @param request
     * @return
     * @throws Exception
     */
    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> login(
            @RequestBody AuthenticationRequestParamDTO request
    ) throws Exception {
        return ResponseEntity.ok(authenticationService.authenticate(request));
    }

    /**
     * Modify User Info Manager Data
     *
     * @param updateUserInfoManagerParamDTO
     * @return
     * @throws Exception
     */
    @PutMapping
    public ResponseEntity<ApiResults> modifyUserInfoManagerData(@RequestBody @Validated UpdateUserInfoManagerParamDTO updateUserInfoManagerParamDTO) throws Exception {

        var updateUserInfoManagerDTO = new UpdateUserInfoManagerDTO();
        updateUserInfoManagerDTO.setId(updateUserInfoManagerParamDTO.getId());
        updateUserInfoManagerDTO.setUsername(updateUserInfoManagerParamDTO.getUsername());
        updateUserInfoManagerDTO.setPassword(updateUserInfoManagerParamDTO.getPassword());
        updateUserInfoManagerDTO.setFirstname(updateUserInfoManagerParamDTO.getFirstname());
        updateUserInfoManagerDTO.setLastname(updateUserInfoManagerParamDTO.getLastname());
        updateUserInfoManagerDTO.setBirthday(updateUserInfoManagerParamDTO.getBirthday());
        updateUserInfoManagerDTO.setAvatarUrl(updateUserInfoManagerParamDTO.getAvatarUrl());
        updateUserInfoManagerDTO.setPhoneNumber(updateUserInfoManagerParamDTO.getPhoneNumber());
        updateUserInfoManagerDTO.setAddress(updateUserInfoManagerParamDTO.getAddress());
        updateUserInfoManagerDTO.setRole(updateUserInfoManagerParamDTO.getRole());

        this.userInfoManagerService.modifyUserInfoManagerData(updateUserInfoManagerDTO);

        var apiResult = new ApiResults<>(true);
        apiResult.setData(updateUserInfoManagerDTO);

        return ResponseEntity.ok(apiResult);
    }

    /**
     * Remove User Info Manager Data
     *
     * @param id
     * @return
     * @throws Exception
     */
    @DeleteMapping(path = "/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ApiResults> removeUserInfoManagerData(@PathVariable("id") Integer id) throws Exception {
        return this.userInfoManagerService.removeUserInfoManagerDataById(id);
    }

    /**
     * Check Duplicate For Username
     * @param username
     * @return
     * @throws Exception
     */
    @GetMapping("check-duplication-username/{username}")
    public ResponseEntity<ApiResults> checkDuplicateForUsername (@PathVariable("username") String username) throws Exception{
        return this.userInfoManagerService.checkDuplicationForUsername(username);
    }

    /**
     * Check Duplicate For Phone Number
     * @param phoneNumber
     * @return
     * @throws Exception
     */
    @GetMapping("check-duplication-phoneNumber/{phoneNumber}")
    public ResponseEntity<ApiResults> checkDuplicateForPhoneNumber (@PathVariable("phoneNumber") String phoneNumber) throws Exception{
        return this.userInfoManagerService.checkDuplicationForPhoneNumber(phoneNumber);
    }
}
