package com.hieupv.service;

import com.hieupv.common.apiResult.ApiResults;
import com.hieupv.dto.api.results.CreateCartManagerDTO;
import com.hieupv.dto.api.results.UpdateCartManagerDTO;
import com.hieupv.entity.CartManagement;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

/**
 * Cart Manager Services
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 16-08-2023
 */
public interface CartManagerServices {
    /**
     * List All Cart Manager Data
     *
     * @return
     * @throws Exception
     */
    public List<CartManagement> listAllCartManagerData() throws Exception;

    /**
     * Get Category Manager Data By Id
     *
     * @param cartManagerId
     * @return
     */
    public CartManagement getCartManagerDataById(int cartManagerId) throws Exception;

    /**
     * Create Cart Manager Data
     *
     * @param createCartManagerDTO
     * @throws Exception
     */
    public void createCartManagerData(CreateCartManagerDTO createCartManagerDTO) throws Exception;

    /**
     * Modify Cart Manager Data
     *
     * @param updateCartManagerDTO
     */
    public void modifyCartManagerData(UpdateCartManagerDTO updateCartManagerDTO) throws Exception;

    /**
     * Remove Cart Manager Data By Id
     *
     * @param id
     */
    public ResponseEntity<ApiResults> removeCartManagerDataById(Integer id) throws Exception;

}
