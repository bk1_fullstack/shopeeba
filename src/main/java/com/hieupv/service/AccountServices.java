package com.hieupv.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Account Services
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 05-08-2023
 */
public interface AccountServices extends UserDetailsService {
}
