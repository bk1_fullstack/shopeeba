package com.hieupv.service;

import com.hieupv.common.apiResult.ApiResults;
import com.hieupv.dto.api.results.CreateCategoryManagerDTO;
import com.hieupv.dto.api.results.CreateUserInfoManagerDTO;
import com.hieupv.dto.api.results.UpdateCategoryManagerDTO;
import com.hieupv.dto.api.results.UpdateUserInfoManagerDTO;
import com.hieupv.entity.AccountInfoManager;
import com.hieupv.entity.CategoryManagement;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

/**
 * Category Manager Services
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 08-08-2023
 */
public interface CategoryManagerServices {
    /**
     * List All Category Manager Data
     *
     * @return
     * @throws Exception
     */
    public List<CategoryManagement> listAllCategoryManagerData() throws Exception;

    /**
     * Get Category Manager Data By Id
     *
     * @param categoryManagerId
     * @return
     */
    public CategoryManagement getCategoryManagerDataById(int categoryManagerId) throws Exception;

    /**
     * Create Category Manager Data
     * @param createCategoryManagerDTO
     * @throws Exception
     */
    public void createCategoryManagerData(CreateCategoryManagerDTO createCategoryManagerDTO) throws Exception;

    /**
     * Modify Category Manager Data
     * @param updateCategoryManagerDTO
     */
    public void modifyCategoryManagerData(UpdateCategoryManagerDTO updateCategoryManagerDTO) throws Exception;

    /**
     * Remove Category Manager Data By Id
     * @param id
     */
    public ResponseEntity<ApiResults> removeCategoryManagerDataById(Integer id) throws Exception;

}
