package com.hieupv.service;

import com.hieupv.dto.api.results.CreateProductManagerDTO;
import com.hieupv.dto.api.results.UpdateProductManagerDTO;
import com.hieupv.entity.ProductManagement;

import java.util.List;

/**
 * Product Manager Services
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 15-08-2023
 */
public interface ProductManagerServices {
    /**
     * List All Product Manager Data
     *
     * @return
     * @throws Exception
     */
    public List<ProductManagement> listAllProductManagerData() throws Exception;

    /**
     * Get Category Manager Data By Id
     *
     * @param productManagerId
     * @return
     */
    public ProductManagement getProductManagerDataById(int productManagerId) throws Exception;

    /**
     * Create Product Manager Data
     * @param createProductManagerDTO
     * @throws Exception
     */
    public void createProductManagerData(CreateProductManagerDTO createProductManagerDTO) throws Exception;

    /**
     * Modify Product Manager Data
     * @param updateProductManagerDTO
     */
    public void modifyProductManagerData(UpdateProductManagerDTO updateProductManagerDTO) throws Exception;

    /**
     * Remove Product Manager Data By Id
     * @param id
     */
    public void removeProductManagerDataById(Integer id) throws Exception;

}
