package com.hieupv.service.impl;

import com.hieupv.common.apiResult.ApiResults;
import com.hieupv.dto.api.results.CreateCartManagerDTO;
import com.hieupv.dto.api.results.CreateUserInfoManagerDTO;
import com.hieupv.dto.api.results.UpdateUserInfoManagerDTO;
import com.hieupv.dto.api.results.ViewUserInfoManagerDTO;
import com.hieupv.entity.AccountInfoManager;
import com.hieupv.repository.UserInfoManagerRepository;
import com.hieupv.service.CartManagerServices;
import com.hieupv.service.UserInfoManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

/**
 * User Info Manager Service Impl
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 04-08-2023
 */

@Service
@RequiredArgsConstructor
public class UserInfoManagerServiceImpl implements UserInfoManagerService {

    private final UserInfoManagerRepository userInfoManagerRepository;
    private final PasswordEncoder passwordEncoder;
    private final CartManagerServices cartManagerServices;

    /**
     * list All User Info Manager Data
     * @return
     * @throws Exception
     */
    @Override
    public List<AccountInfoManager> listAllUserInfoManagerData() throws Exception {
        return this.userInfoManagerRepository.findAll();
    }

    /**
     * Get User Info Manager Data By Id
     * @param userId
     * @return
     */
    @Override
    public AccountInfoManager getUserInfoManagerDataById(int userId) {
        Optional<AccountInfoManager> accountInfoManager = this.userInfoManagerRepository.findById(userId);
        return accountInfoManager.get();
    }

    /**
     * Create User Info Manager Data
     * @param createUserInfoManagerDTO
     * @throws Exception
     */
    @Override
    public void createUserInfoManagerData(CreateUserInfoManagerDTO createUserInfoManagerDTO) throws Exception{

//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        UserDetails loginUserDetails = (UserDetails) authentication.getPrincipal();

        createUserInfoManagerDTO.setModifyByUserId(1);

        this.userInfoManagerRepository.createUserInfoManager(createUserInfoManagerDTO);

        Integer userId = userInfoManagerRepository.findByUsername(createUserInfoManagerDTO.getUsername()).get().getId();

        CreateCartManagerDTO createCartManagerDTO = new CreateCartManagerDTO();

        createCartManagerDTO.setQuantityProduct(0);
        createCartManagerDTO.setTotalCount(0);
        createCartManagerDTO.setTotalPrice(0);
        createCartManagerDTO.setAccountId(userId);

        this.cartManagerServices.createCartManagerData(createCartManagerDTO);
    }

    /**
     * save
     * @param accountInfoManager
     */
    @Override
    public void save(AccountInfoManager accountInfoManager) throws Exception {
        this.userInfoManagerRepository.save(accountInfoManager);
    }

    /**
     * Find By Username
     * @param username
     * @return
     * @throws Exception
     */
    @Override
    public Optional<AccountInfoManager> findByUsername(String username) throws Exception {
        return this.userInfoManagerRepository.findByUsername(username);
    }

    /**
     * Modify User Info Manager Data
     * @param updateUserInfoManagerDTO
     */
    @Override
    public void modifyUserInfoManagerData(UpdateUserInfoManagerDTO updateUserInfoManagerDTO) throws Exception{
        Optional<AccountInfoManager> viewUserInfoManagerDTO= userInfoManagerRepository.findById(updateUserInfoManagerDTO.getId());

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails loginUserDetails = (UserDetails) authentication.getPrincipal();
        updateUserInfoManagerDTO.setModifyByUserId(userInfoManagerRepository.findByUsername(loginUserDetails.getUsername()).get().getId());

        if (updateUserInfoManagerDTO.getPassword() != null && !updateUserInfoManagerDTO.getPassword().isEmpty()){
                updateUserInfoManagerDTO.setPassword(passwordEncoder.encode(updateUserInfoManagerDTO.getPassword()));
        }
        else {
            updateUserInfoManagerDTO.setPassword(viewUserInfoManagerDTO.get().getPassword());
        }

        if (updateUserInfoManagerDTO.getUsername() != null && !updateUserInfoManagerDTO.getUsername().isEmpty()){
                updateUserInfoManagerDTO.setUsername(updateUserInfoManagerDTO.getUsername());
        }
        else {
            updateUserInfoManagerDTO.setUsername(viewUserInfoManagerDTO.get().getUsername());
        }
        if (updateUserInfoManagerDTO.getRole() != null && !updateUserInfoManagerDTO.getRole().isEmpty()){
                updateUserInfoManagerDTO.setRole(updateUserInfoManagerDTO.getRole());
        }
        else {
            updateUserInfoManagerDTO.setRole(viewUserInfoManagerDTO.get().getRole());
        }

        this.userInfoManagerRepository.modifyUserInfoManager(updateUserInfoManagerDTO);

    }

    /**
     * Remove User Info Manager Data By Id
     * @param id
     */
    @Override
    public ResponseEntity<ApiResults> removeUserInfoManagerDataById(Integer id) throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails loginUserDetails = (UserDetails) authentication.getPrincipal();

        Optional<AccountInfoManager> accountInfoManager = userInfoManagerRepository.findById(id);

        if (accountInfoManager != null) {
            if (loginUserDetails.getUsername().equals(accountInfoManager.get().getUsername())) {
                return ResponseEntity.ok(new ApiResults(false));
            } else {
                int deletedRow = this.userInfoManagerRepository.deleteUserById(id);
                return ResponseEntity.ok(new ApiResults(true));
            }
        }

        return ResponseEntity.notFound().build();
    }

    /**
     * Check Duplication For Username
     * @param username
     * @return
     */
    @Override
    public ResponseEntity<ApiResults> checkDuplicationForUsername(String username) throws Exception {
        Optional<AccountInfoManager> accountInfoManager= userInfoManagerRepository.findByUsername(username);
        if (accountInfoManager != null && !accountInfoManager.isEmpty()){
            var results = new ApiResults<>();
            results.setSuccess(true);
            return ResponseEntity.ok(results);
        }
        var results = new ApiResults<>();
        results.setSuccess(false);
        return ResponseEntity.ok(results);
    }

    /**
     * Check Duplication For Phone Number
     * @param phoneNumber
     * @return
     * @throws Exception
     */
    @Override
    public ResponseEntity<ApiResults> checkDuplicationForPhoneNumber(String phoneNumber) throws Exception {
        Optional<AccountInfoManager> accountInfoManager= userInfoManagerRepository.findByPhoneNumber(phoneNumber);
        if (accountInfoManager != null && !accountInfoManager.isEmpty()){
            var results = new ApiResults<>();
            results.setSuccess(true);
            return ResponseEntity.ok(results);
        }
        var results = new ApiResults<>();
        results.setSuccess(false);
        return ResponseEntity.ok(results);
    }
}
