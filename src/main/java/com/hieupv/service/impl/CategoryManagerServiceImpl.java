package com.hieupv.service.impl;

import com.hieupv.common.apiResult.ApiResults;
import com.hieupv.dto.api.results.CreateCategoryManagerDTO;
import com.hieupv.dto.api.results.CreateUserInfoManagerDTO;
import com.hieupv.dto.api.results.UpdateCategoryManagerDTO;
import com.hieupv.dto.api.results.UpdateUserInfoManagerDTO;
import com.hieupv.entity.AccountInfoManager;
import com.hieupv.entity.CategoryManagement;
import com.hieupv.repository.CategoryManagerRepository;
import com.hieupv.repository.UserInfoManagerRepository;
import com.hieupv.service.CategoryManagerServices;
import com.hieupv.service.UserInfoManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Category Manager Service Impl
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 04-08-2023
 */

@Service
@RequiredArgsConstructor
public class CategoryManagerServiceImpl implements CategoryManagerServices {
    private final UserInfoManagerService userInfoManagerService;
    private final CategoryManagerRepository categoryManagerRepository;
    private final UserInfoManagerRepository userInfoManagerRepository;

    /**
     * list All User Info Manager Data
     * @return
     * @throws Exception
     */
    @Override
    public List<CategoryManagement> listAllCategoryManagerData() throws Exception {
        return this.categoryManagerRepository.findAll();
    }

    /**
     * Get Category Manager Data By Id
     * @param categoryManagerId
     * @return
     */
    @Override
    public CategoryManagement getCategoryManagerDataById(int categoryManagerId) {
        Optional<CategoryManagement> categoryManagement = this.categoryManagerRepository.findById(categoryManagerId);
        return categoryManagement.get();
    }

    /**
     * Create Category Manager Data
     * @param createCategoryManagerDTO
     * @throws Exception
     */
    @Override
    public void createCategoryManagerData(CreateCategoryManagerDTO createCategoryManagerDTO) throws Exception{

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails loginUserDetails = (UserDetails) authentication.getPrincipal();

        createCategoryManagerDTO.setModifyByUserId(userInfoManagerService.findByUsername(loginUserDetails.getUsername()).get().getId());

        this.categoryManagerRepository.createCategoryManager(createCategoryManagerDTO);
    }

    /**
     * Modify Category Manager Data
     * @param updateCategoryManagerDTO
     */
    @Override
    public void modifyCategoryManagerData(UpdateCategoryManagerDTO updateCategoryManagerDTO) throws Exception{
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails loginUserDetails = (UserDetails) authentication.getPrincipal();
        updateCategoryManagerDTO.setModifyByUserId(userInfoManagerRepository.findByUsername(loginUserDetails.getUsername()).get().getId());

        this.categoryManagerRepository.modifyCategoryManager(updateCategoryManagerDTO);
    }

    /**
     * Remove Category Manager Data By Id
     * @param id
     */
    @Override
    public ResponseEntity<ApiResults> removeCategoryManagerDataById(Integer id) throws Exception {
        Optional<CategoryManagement> categoryManagement= categoryManagerRepository.findById(id);

        if (categoryManagement != null) {
                int deletedRow = this.categoryManagerRepository.deleteCategoryManagementById(id);
                return ResponseEntity.ok(new ApiResults(true));
        }
        return ResponseEntity.notFound().build();
    }
}
