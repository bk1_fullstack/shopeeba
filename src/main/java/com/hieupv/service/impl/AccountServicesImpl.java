package com.hieupv.service.impl;

import com.hieupv.entity.AccountInfoManager;
import com.hieupv.repository.UserInfoManagerRepository;
import com.hieupv.service.AccountServices;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Account Services Impl
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 05-08-2023
 */
@Service
@RequiredArgsConstructor
public class AccountServicesImpl implements AccountServices {
    private final UserInfoManagerRepository userInfoManagerRepository;

    /**
     * Load User By Username
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<AccountInfoManager> optionalAccount = userInfoManagerRepository.findByUsername(username);
        if (optionalAccount.isEmpty()) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(optionalAccount.get().getRole());
        return new User(optionalAccount.get().getUsername(), optionalAccount.get().getPassword(), authorities);
    }
}
