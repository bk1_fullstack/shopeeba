package com.hieupv.service.impl;

import com.hieupv.common.apiResult.ApiResults;
import com.hieupv.dto.api.results.CreateCartManagerDTO;
import com.hieupv.dto.api.results.UpdateCartManagerDTO;
import com.hieupv.entity.CartManagement;
import com.hieupv.entity.ProductManagement;
import com.hieupv.repository.CartManagerRepository;
import com.hieupv.service.CartManagerServices;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Cart Manager Service Impl
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 18-08-2023
 */

@Service
@RequiredArgsConstructor
public class CartManagerServiceImpl implements CartManagerServices {
    /**
     * Cart Manager Repository
     */
    private final CartManagerRepository cartManagerRepository;

    /**
     * list All Cart Manager Data
     *
     * @return
     * @throws Exception
     */
    @Override
    public List<CartManagement> listAllCartManagerData() throws Exception {
        return this.cartManagerRepository.findAll();
    }

    /**
     * Get Category Manager Data By Id
     *
     * @param cartManagerId
     * @return
     */
    @Override
    public CartManagement getCartManagerDataById(int cartManagerId) throws Exception {
        Optional<CartManagement> cartManagement = this.cartManagerRepository.findById(cartManagerId);
        return cartManagement.get();
    }

    /**
     * Create Cart Manager Data
     *
     * @param createCartManagerDTO
     * @throws Exception
     */
    @Override
    public void createCartManagerData(CreateCartManagerDTO createCartManagerDTO) throws Exception {

        this.cartManagerRepository.createCartManager(createCartManagerDTO);
    }

    /**
     * Modify Cart Manager Data
     *
     * @param updateCartManagerDTO
     */
    @Override
    public void modifyCartManagerData(UpdateCartManagerDTO updateCartManagerDTO) throws Exception {
        int count = updateCartManagerDTO.getProductManagements().size();

        updateCartManagerDTO.setTotalCount(count);

        float price = 0;

        List<ProductManagement> productManagements = updateCartManagerDTO.getProductManagements();

        for (ProductManagement product : productManagements
        ) {

            price = product.getPrice();
            updateCartManagerDTO.setTotalPrice(price * updateCartManagerDTO.getQuantityProduct());
        }

        this.cartManagerRepository.modifyCartManager(updateCartManagerDTO);
    }

    /**
     * Remove Cart Manager Data By Id
     *
     * @param id
     */
    @Override
    public ResponseEntity<ApiResults> removeCartManagerDataById(Integer id) throws Exception {
        Optional<CartManagement> cartManagement = cartManagerRepository.findById(id);

        if (cartManagement != null) {
            int deletedRow = this.cartManagerRepository.deleteCartManagementById(id);
            return ResponseEntity.ok(new ApiResults(true));
        }
        return ResponseEntity.notFound().build();
    }
}
