package com.hieupv.service.impl;

import com.hieupv.dto.api.results.CreateProductManagerDTO;
import com.hieupv.dto.api.results.UpdateProductManagerDTO;
import com.hieupv.entity.ProductManagement;
import com.hieupv.repository.ProductManagerRepository;
import com.hieupv.repository.UserInfoManagerRepository;
import com.hieupv.service.ProductManagerServices;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ProductManagerServicesImpl implements ProductManagerServices {
    private final ProductManagerRepository productManagerRepository;
    private final UserInfoManagerRepository userInfoManagerRepository;

    /**
     * List All Product Manager Data
     * @return
     * @throws Exception
     */
    @Override
    public List<ProductManagement> listAllProductManagerData() throws Exception {
        return productManagerRepository.findAll();
    }

    /**
     * Get Product Manager Data By Id
     * @param productManagerId
     * @return
     * @throws Exception
     */
    @Override
    public ProductManagement getProductManagerDataById(int productManagerId) throws Exception {
        Optional<ProductManagement>  product= productManagerRepository.findById(productManagerId);
        return product.get();
    }

    /**
     * Create Product Manager Data
     * @param createProductManagerDTO
     * @throws Exception
     */
    @Override
    public void createProductManagerData(CreateProductManagerDTO createProductManagerDTO) throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails loginUserDetails = (UserDetails) authentication.getPrincipal();

        createProductManagerDTO.setModifyByUserId(userInfoManagerRepository.findByUsername(loginUserDetails.getUsername()).get().getId());

        productManagerRepository.createProductManager(createProductManagerDTO);
    }

    /**
     * Modify Product Manager Data
     * @param updateProductManagerDTO
     * @throws Exception
     */
    @Override
    public void modifyProductManagerData(UpdateProductManagerDTO updateProductManagerDTO) throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails loginUserDetails = (UserDetails) authentication.getPrincipal();

        updateProductManagerDTO.setModifyByUserId(userInfoManagerRepository.findByUsername(loginUserDetails.getUsername()).get().getId());

        productManagerRepository.modifyProductManager(updateProductManagerDTO);
    }

    /**
     * Remove Product Manager Data By Id
     * @param id
     * @throws Exception
     */
    @Override
    public void removeProductManagerDataById(Integer id) throws Exception {
        productManagerRepository.deleteProductManagementById(id);
    }
}
