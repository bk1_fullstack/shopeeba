package com.hieupv.service;

import com.hieupv.common.apiResult.ApiResults;
import com.hieupv.dto.api.results.CreateUserInfoManagerDTO;
import com.hieupv.dto.api.results.UpdateUserInfoManagerDTO;
import com.hieupv.entity.AccountInfoManager;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

/**
 * User Info Manager Service
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 04-08-2023
 */

public interface UserInfoManagerService {
    /**
     * List All User Info Manager Data
     *
     * @return
     * @throws Exception
     */
    public List<AccountInfoManager> listAllUserInfoManagerData() throws Exception;

    /**
     * Get User Info Manager Data By Id
     *
     * @param userId
     * @return
     */
    public AccountInfoManager getUserInfoManagerDataById(int userId) throws Exception;

    /**
     * Create User Info Manager Data
     * @param createUserInfoManagerDTO
     * @throws Exception
     */
    public void createUserInfoManagerData(CreateUserInfoManagerDTO createUserInfoManagerDTO) throws Exception;

    /**
     * save
     * @param accountInfoManager
     */
    public void save(AccountInfoManager accountInfoManager) throws Exception;

    /**
     * Find By Username
     * @param username
     * @return
     * @throws Exception
     */
    public Optional<AccountInfoManager> findByUsername(String username) throws Exception;

    /**
     * Modify User Info Manager Data
     * @param updateUserInfoManagerDTO
     */
    public void modifyUserInfoManagerData(UpdateUserInfoManagerDTO updateUserInfoManagerDTO) throws Exception;

    /**
     * Remove User Info Manager Data By Id
     * @param id
     */
    public ResponseEntity<ApiResults> removeUserInfoManagerDataById(Integer id) throws Exception;

    /**
     * Check Duplication For Username
     * @param username
     * @return
     */
    public ResponseEntity<ApiResults> checkDuplicationForUsername(String username) throws Exception;

    /**
     * Check Duplication For Phone Number
     * @param phoneNumber
     * @return
     * @throws Exception
     */
    public ResponseEntity<ApiResults> checkDuplicationForPhoneNumber(String phoneNumber) throws Exception;
}
