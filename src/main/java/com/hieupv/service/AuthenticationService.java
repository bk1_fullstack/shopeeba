package com.hieupv.service;

import com.hieupv.config.configJwt.Jwtservice;
import com.hieupv.dto.api.AuthenticationResponse;
import com.hieupv.dto.api.param.AuthenticationRequestParamDTO;
import com.hieupv.dto.api.results.CreateUserInfoManagerDTO;
import com.hieupv.entity.AccountInfoManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Authentication Service
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 06-08-2023
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class AuthenticationService {
    private final UserInfoManagerService userInfoManagerService;
    private final Jwtservice jwtservice;
    private final AuthenticationManager authenticationManager;

    /**
     * authenticate
     * @param request
     * @return
     * @throws Exception
     */
    public AuthenticationResponse authenticate(AuthenticationRequestParamDTO request) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            request.getUsername(),
                            request.getPassword()
                    )
            );

            var user =userInfoManagerService.findByUsername(request.getUsername())
                    .orElseThrow(()-> new UsernameNotFoundException("Username not found"));
            var jwtToken = jwtservice.generateToken(user);
            return AuthenticationResponse.builder()
                    .token(jwtToken)
                    .build();
        }
        catch (Exception e){
            e.getMessage();
            System.out.println(e.getMessage());
            return null;
        }
        }

}
