package com.hieupv.common.apiResult;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Api Results
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 04-08-2023
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiResults<T> {
    /**
     * success
     */
    private boolean success;

    /**
     * Data
     */
    private T Data;

    /**
     * Constructor ApiResults With Success
     * @param isSuccess
     */
    public ApiResults(boolean isSuccess) {
        this.success = isSuccess;
    }

}
