package com.hieupv.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * Category Information
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 07-08-2023
 */
@Data
@Entity
public class CategoryManagement {
    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    private Integer id;

    /**
     * Category Name
     */
    @NotNull
    @Size(max = 255)
    private String categoryName;

    /**
     * Create Date
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    /**
     * Modify Date
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifyDate;

    /**
     * Modify By User Id
     */
    @NotNull
    private Integer modifyByUserId;

    /**
     * Join Product
     */
    @OneToMany(mappedBy = "categoryId")
    private List<ProductManagement> productManagements;

}
