package com.hieupv.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Account Information
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 04-08-2023
 */

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "AccountInfoManager")
public class AccountInfoManager implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    /**
     * Account id
     */
    @NotNull
    private Integer id;

    /**
     * Username
     */
    @Size(max = 50)
    @NotNull
    private String username;

    /**
     * Password
     */
    @NotNull
    private String password;

    /**
     * First Name
     */
    @Size(max = 50)
    @NotNull
    private String firstname;

    /**
     * Last Name
     */
    @Size(max = 50)
    @NotNull
    private String lastname;

    /**
     * Birth Day
     */
    @NotNull
    @Temporal(TemporalType.DATE)
    private Date birthday;

    /**
     * Avatar Url
     */
    @Nullable
    private String avatarUrl;

    /**
     * Avatar Name
     */
    @Nullable
    private String avatarName;

    /**
     * Phone Number
     */
    @NotNull
    @Size(max = 20)
    private String phoneNumber;

    /**
     * Address
     */
    @Size(max = 255)
    @NotNull
    private String address;

    /**
     * Create Date
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    /**
     * Modify Date
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifyDate;

    /**
     * Modify By User Id
     */
    private Integer modifyByUserId;

    /**
     * Role
     */
    @NotNull
    @Size(max = 10)
    private String role;

    /**
     * setRole
     *
     * @param role
     */
    public void setRole(String role) {
        if (role != null) {
            if (role.equals("ADMIN") || role.equals("USER")) {
                this.role = role;
            } else {
                throw new IllegalArgumentException("Role must be either ADMIN or USER");
            }
        } else {
            throw new IllegalArgumentException("Role cannot be null");
        }
    }

    /**
     * Set Role For UserDetail
     *
     * @return
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(role));
    }

    /**
     * Setting Expired For UserDetail
     *
     * @return
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Set User Non Locked
     *
     * @return
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * Set User Credentials Non Expired
     *
     * @return
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * Active User
     *
     * @return
     */
    @Override
    public boolean isEnabled() {
        return true;
    }

    /**
     * Join Cart
     */
    @OneToOne(mappedBy = "accountInfoManager")
    private CartManagement cartManagement;
}
