package com.hieupv.entity;

import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Product Information
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 07-08-2023
 */
@Data
@Entity
public class ProductManagement {
    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    private Integer id;

    /**
     * Product Name
     */
    @NotNull
    @Size(max = 255)
    private String productName;

    /**
     * Description
     */
    @NotNull
    @Size(max = 255)
    private String description;

    /**
     * Description
     */
    @NotNull
    private float price;

    /**
     * Quantity Sold
     */
    @NotNull
    private Integer quantitySold;

    /**
     * Stock
     */
    @NotNull
    private Integer stock;

    /**
     * Create Date
     */
    @NotNull
    @Size(max = 255)
    private String createDate;

    /**
     * Modify Date
     */
    @NotNull
    @Size(max = 255)
    private String modifyDate;

    /**
     * Modify By User Id
     */
    @NotNull
    private Integer modifyByUserId;

    /**
     * Join With Category
     */
    @ManyToOne
    @JoinColumn(name = "category_id")
    private CategoryManagement categoryId;

    /**
     * Join Cart
     */
    @ManyToOne
    @JoinColumn(name = "cart_id")
    private CartManagement cartManagement;

}
