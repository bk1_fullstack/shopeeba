package com.hieupv.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Cart Management Information
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 07-08-2023
 */
@Entity
@Data
public class CartManagement {
    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    private Integer id;

    /**
     * Total Price
     */
    @NotNull
    private float totalPrice;

    /**
     * Total count
     */
    @NotNull
    private Integer totalCount;

    /**
     * Quantity Product
     */
    @NotNull
    private Integer quantityProduct;

    /**
     * Join Product
     */
    @JsonManagedReference
    @OneToMany(mappedBy = "cartManagement")
    private List<ProductManagement> productManagements;

    /**
     * Account Info Manager
     */
    @JsonBackReference
    @OneToOne
    @JoinColumn(name = "account_id")
    private AccountInfoManager accountInfoManager;

}
