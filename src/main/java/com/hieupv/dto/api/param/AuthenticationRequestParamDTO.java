package com.hieupv.dto.api.param;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Authentication Request Param DTO
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 06-08-2023
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationRequestParamDTO {
    /**
     * username
     */
    @NotNull
    @Size(max = 50)
    private String username;

    /**
     * password
     */
    @NotNull
    private String password;
}
