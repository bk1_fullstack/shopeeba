package com.hieupv.dto.api.param;

import lombok.Data;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Create Category Manager Param DTO
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 08-08-2023
 */

@Data
public class CreateCategoryManagerParamDTO {

    /**
     * Category Name
     */
    @Size(max = 255)
    @NotNull
    private String categoryName;

}
