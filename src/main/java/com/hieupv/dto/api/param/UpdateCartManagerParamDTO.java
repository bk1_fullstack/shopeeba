package com.hieupv.dto.api.param;

import com.hieupv.entity.ProductManagement;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Update Cart Manager Param DTO
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 18-08-2023
 */

@Data
public class UpdateCartManagerParamDTO {
    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    private Integer id;

    /**
     * Quantity Product
     */
    @NotNull
    private Integer quantityProduct;

    /**
     * Join Product
     */
    private List<ProductManagement> productManagements;

}
