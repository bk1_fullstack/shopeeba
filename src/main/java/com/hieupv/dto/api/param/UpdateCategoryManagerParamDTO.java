package com.hieupv.dto.api.param;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Update Category Manager Param DTO
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 08-08-2023
 */

@Data
public class UpdateCategoryManagerParamDTO {
    /**
     *  id
     */
    @NotNull
    private Integer id;

    /**
     * Category Name
     */
    @Size(max = 255)
    @NotNull
    private String categoryName;

}
