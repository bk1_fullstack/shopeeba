package com.hieupv.dto.api.param;

import com.hieupv.entity.ProductManagement;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Create Cart Manager Param DTO
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 18-08-2023
 */

@Data
public class CreateCartManagerParamDTO {
    /**
     * Account Id
     */
    private Integer accountId;
    /**
     * Join Product
     */
    private List<ProductManagement> productManagements;

}
