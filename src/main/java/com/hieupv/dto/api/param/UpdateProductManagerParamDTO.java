package com.hieupv.dto.api.param;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Create Product Manager Param DTO
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 15-08-2023
 */

@Data
public class UpdateProductManagerParamDTO {
    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    private Integer id;

    /**
     * Product Name
     */
    @NotNull
    @Size(max = 255)
    private String productName;

    /**
     * Description
     */
    @NotNull
    @Size(max = 255)
    private String description;

    /**
     * Description
     */
    @NotNull
    private float price;

    /**
     * Stock
     */
    @NotNull
    private Integer stock;

    /**
     * Category Id
     */
    @NotNull
    private Integer categoryId;

}
