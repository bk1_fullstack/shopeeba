package com.hieupv.dto.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Authentication Response
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 06-08-2023
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationResponse {
    /**
     * Token
     */
    @NotNull
    private String token;

}
