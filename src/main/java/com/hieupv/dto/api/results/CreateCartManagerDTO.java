package com.hieupv.dto.api.results;

import com.hieupv.entity.AccountInfoManager;
import com.hieupv.entity.ProductManagement;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Create Cart Manager DTO
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 18-08-2023
 */

@Data
public class CreateCartManagerDTO {
    /**
     * Total Price
     */
    private float totalPrice;

    /**
     * Total count
     */
    private Integer totalCount;

    /**
     * Quantity Product
     */
    private Integer quantityProduct;

    /**
     * Join Product
     */
    private List<ProductManagement> productManagements;

    /**
     * Account Info Manager
     */
    @NotNull
    private Integer accountId;

}
