package com.hieupv.dto.api.results;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * View List Product Manager DTO
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 08-08-2023
 */

@Data
public class ViewListProductManagerDTO {
    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    private Integer id;

    /**
     * Product Name
     */
    @NotNull
    @Size(max = 255)
    private String productName;

    /**
     * Description
     */
    @NotNull
    @Size(max = 255)
    private String description;

    /**
     * Description
     */
    @NotNull
    private float price;

    /**
     * Quantity Sold
     */
    @NotNull
    private Integer quantitySold;

    /**
     * Stock
     */
    @NotNull
    private Integer stock;

    /**
     * Create Date
     */
    @NotNull
    @Size(max = 255)
    private String createDate;

    /**
     * Modify Date
     */
    @NotNull
    @Size(max = 255)
    private String modifyDate;

    /**
     * Modify By User Id
     */
    @NotNull
    private Integer modifyByUserId;

}
