package com.hieupv.dto.api.results;

import lombok.Data;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Update Category Manager DTO
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 08-08-2023
 */

@Data
public class UpdateCategoryManagerDTO {
    /**
     *  id
     */
    @NotNull
    private Integer id;

    /**
     * Category Name
     */
    @Size(max = 255)
    @NotNull
    private String categoryName;

    /**
     * Modify By User Id
     */
    @NotNull
    private Integer modifyByUserId;

    /**
     * Modify Date
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifyDate;

}
