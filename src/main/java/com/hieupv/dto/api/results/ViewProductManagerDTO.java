package com.hieupv.dto.api.results;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * View Product Manager DTO
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 15-08-2023
 */

@Data
public class ViewProductManagerDTO {
    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    private Integer id;

    /**
     * Product Name
     */
    @NotNull
    @Size(max = 255)
    private String productName;

    /**
     * Description
     */
    @NotNull
    @Size(max = 255)
    private String description;

    /**
     * Description
     */
    @NotNull
    private float price;

    /**
     * Quantity Sold
     */
    @NotNull
    private Integer quantitySold;

    /**
     * Stock
     */
    @NotNull
    private Integer stock;

    /**
     * Create Date
     */
    @NotNull
    @Size(max = 255)
    private String createDate;

    /**
     * Modify Date
     */
    @NotNull
    @Size(max = 255)
    private String modifyDate;

    /**
     * Modify By User Id
     */
    @NotNull
    private Integer modifyByUserId;

}
