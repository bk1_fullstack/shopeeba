package com.hieupv.dto.api.results;

import com.hieupv.entity.AccountInfoManager;
import com.hieupv.entity.ProductManagement;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * View Cart Manager DTO
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 18-08-2023
 */

@Data
public class ViewCartManagerDTO {
    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    private Integer id;

    /**
     * Total Price
     */
    @NotNull
    private float totalPrice;

    /**
     * Total count
     */
    @NotNull
    private Integer totalCount;

    /**
     * Quantity Product
     */
    @NotNull
    private Integer quantityProduct;

    /**
     * Join Product
     */
    private List<ProductManagement> productManagements;

    /**
     * Account Info Manager
     */
    private AccountInfoManager accountInfoManager;

}
