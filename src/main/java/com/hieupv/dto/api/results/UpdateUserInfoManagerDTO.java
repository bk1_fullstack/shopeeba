package com.hieupv.dto.api.results;

import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Create User Info Manager DTO
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 04-08-2023
 */

@Data
public class UpdateUserInfoManagerDTO {
    /**
     * Account id
     */
    @NotNull
    private Integer id;

    /**
     * Username
     */
    @Size(max = 50)
    @NotNull
    private String username;

    /**
     * Password
     */
    @NotNull
    private String password;

    /**
     * First Name
     */
    @Size(max = 50)
    @NotNull
    private String firstname;

    /**
     * Last Name
     */
    @Size(max = 50)
    @NotNull
    private String lastname;

    /**
     * Birth Day
     */
    @NotNull
    @Temporal(TemporalType.DATE)
    private Date birthday;

    /**
     * Avatar Url
     */
    @Nullable
    private String avatarUrl;

    /**
     * Phone Number
     */
    @NotNull
    @Size(max= 20)
    private String phoneNumber;

    /**
     * Address
     */
    @Size(max = 255)
    @NotNull
    private String address;

    /**
     * Modify Date
     */
    @NotNull
    private Date modifyDate;

    /**
     * Modify By User Id
     */
    @NotNull
    private Integer modifyByUserId;

    /**
     * Role
     */
    @NotNull
    @Size(max = 10)
    private String role;

    public void setRole(String role) {
        if (role != null) {
            if (role.equals("ADMIN") || role.equals("USER")) {
                this.role = role;
            } else {
                throw new IllegalArgumentException("Role must be either ADMIN or USER");
            }
        } else {
            throw new IllegalArgumentException("Role cannot be null");
        }
    }
}
