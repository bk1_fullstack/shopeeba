package com.hieupv.config.configJwt;

import com.hieupv.service.AccountServices;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Jwt Authentication Filter
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 05-08-2023
 */
@Component
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {
    private final Jwtservice jwtservice;
    private final AccountServices accountServices;

    /**
     * Do Filter Internal
     * @param request
     * @param response
     * @param filterChain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(
           @NonNull HttpServletRequest request,
           @NonNull HttpServletResponse response,
           @NonNull FilterChain filterChain)
            throws ServletException, IOException {
            final String authHeader= request.getHeader("Authorization"); // tạo header chứa token jwt

            final String jwt;
            final String username;
                // kiểm tra xem có header hay không ?
                if (authHeader == null || !authHeader.startsWith("Bearer")){
                    filterChain.doFilter(request, response); // chuyển yêu cầu cho bộ lọc tiếp theo
                    return;
                }
                jwt = authHeader.substring(7); // lấy ra mã token từ header với khoảng cách là bỏ 7 ký tự đầu
                username= jwtservice.extracUsername(jwt); // to do to extract username from jwt Token;
                logger.info(username);
                if (username != null && SecurityContextHolder.getContext().getAuthentication() == null){
                    UserDetails userDetails= accountServices.loadUserByUsername(username);
                    if (jwtservice.isTokenValid(jwt, userDetails)){
                        UsernamePasswordAuthenticationToken authenticationToken= new UsernamePasswordAuthenticationToken(
                                userDetails,
                                null,
                                userDetails.getAuthorities()
                        );
                        authenticationToken.setDetails(
                                new WebAuthenticationDetailsSource().buildDetails(request)
                        );
                        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                    }
                }
                filterChain.doFilter(request, response);

    }
}
