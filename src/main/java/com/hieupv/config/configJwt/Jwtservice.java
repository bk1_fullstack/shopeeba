package com.hieupv.config.configJwt;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import io.jsonwebtoken.Claims;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

/**
 * Jwt service
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 05-08-2023
 */
@Service
public class Jwtservice {
    private static final String SECRET_KEY="+tj4+z5aa7J7Qlv22tSWHtW+KbNK+dzMBF3V9Xx+iQ6K76NBzBPpjPv/oBEHlP0p\n";
    public String extracUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    /**
     * Extract Claim
     * @param token
     * @param claimsResolver
     * @return
     * @param <T>
     */
    public <T> T extractClaim(String token , Function<Claims,T> claimsResolver){
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    /**
     * Generate Token
     * @param userDetails
     * @return
     */
    public String generateToken(UserDetails userDetails){
        return generateToken(new HashMap<>(), userDetails);
    }

    /**
     * Generate Token
     * @param extraClaims
     * @param userDetails
     * @return
     */
    public String generateToken(
            Map<String, Objects> extraClaims,
            UserDetails userDetails
    ){
        return Jwts
                .builder() //tạo một đối tượng JwtBuilder để xây dựng JWT.
                .setClaims(extraClaims) //tạo một đối tượng JwtBuilder để xây dựng JWT.
                .setSubject(userDetails.getUsername()) // thiết lập trường "sub" (subject) của JWT là tên người dùng từ đối tượng userDetails.
                .setIssuedAt(new Date(System.currentTimeMillis())) //thiết lập thời điểm phát hành (issued at) của JWT là thời điểm hiện tại.
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 24)) //thiết lập thời điểm phát hành (issued at) của JWT là thời điểm hiện tại.
                .signWith(getSignInKey(), SignatureAlgorithm.HS256) // ký (sign) JWT bằng cách sử dụng chìa khóa getSignInKey() và thuật toán mã hóa HS256. Điều này đảm bảo tính toàn vẹn và xác thực của JWT.
                .compact(); // tạo ra một chuỗi JWT hoàn chỉnh.
    }

    /**
     * Is Token Valid
     * @param token
     * @param userDetails
     * @return
     */
    public boolean isTokenValid(String token, UserDetails userDetails){
       final String username= extracUsername(token);
        return (username.equals(userDetails.getUsername())) && !isTokenExipred((token));
    }

    /**
     * Is Token Exipred
     * @param token
     * @return
     */
    private boolean isTokenExipred(String token) {
        return extractExpiration(token).before(new Date());
    }

    /**
     * Extract Expiration
     * @param token
     * @return
     */
    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    /**
     * Extract All Claims
     * @param token
     * @return
     */
    private Claims extractAllClaims(String token){
        return Jwts
                .parserBuilder()
                .setSigningKey(getSignInKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    /**
     * Get SignIn Key
     * @return
     */
    private Key getSignInKey() {
        byte[] KeyBytes= Decoders.BASE64.decode(SECRET_KEY);
        return Keys.hmacShaKeyFor(KeyBytes);
    }


}
