package com.hieupv.config.configSwagger;

import com.google.common.base.Predicates;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.ServletContext;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Config Swagger
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 05-08-2023
 */

@Configuration
@EnableSwagger2
@RequiredArgsConstructor
public class ConfigSwagger {
    private final ServletContext servletContext;

    /**
     * Docket Api
     *
     * @return
     */
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)//
                .apiInfo(metadata())//
//                .host(server + ":" + port)
//                .pathProvider(new RelativePathProvider(servletContext) {
//                    @Override
//                    public String getApplicationBasePath() {
//                        return basePath;
//                    }
//                })
                .globalOperationParameters(globalParameterList())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(Predicates.not(PathSelectors.regex("/error")))
                .build()
                .useDefaultResponseMessages(false)
                .securitySchemes(Collections.singletonList(apiKey()))
                .securityContexts(Collections.singletonList(securityContext()))
                .genericModelSubstitutes(Optional.class);

    }

    /**
     * Global Parameter List
     *
     * @return
     */
    private List<Parameter> globalParameterList() {

//        Parameter authMasterUnitIdHeader =
//                new ParameterBuilder()
//                        .name("Tenant-Id") // name of the header
//                        .modelRef(new ModelRef("string")) // data-type of the header
//                        .required(false) // required/optional
//                        .parameterType("header") // for query-param, this value can be 'query'
//                        .description("Tenant Id")
//                        .build();

//        return Collections.singletonList(authMasterUnitIdHeader);
        return Collections.emptyList();
    }

    /**
     * metadata
     *
     * @return
     */
    private ApiInfo metadata() {
        return new ApiInfoBuilder()//
                .title("Hieupv - Project management api document")//
                .description("This Is Project From Hieupv, you should click on the right top button Authorize and introduce it with the prefix \"Bearer \".")//
                .version("1.0.0")//
                .license("MIT License").licenseUrl("http://opensource.org/licenses/MIT")//
                .contact(new Contact("hieupv", null, "hieu.th998@gmail.com"))//
                .build();
    }

    /**
     * apiKey
     *
     * @return
     */
    private ApiKey apiKey() {
        return new ApiKey("Authorization", "Authorization", "header");
    }

    /**
     * Security Context
     *
     * @return
     */
    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.any())
                .build();
    }

    /**
     * Default Auth
     *
     * @return
     */
    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Arrays.asList(new SecurityReference("Authorization", authorizationScopes));
    }

}
