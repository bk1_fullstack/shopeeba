package com.hieupv.config.configSecurity;

import com.hieupv.service.AccountServices;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Application Config
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 05-08-2023
 */

@Configuration
@RequiredArgsConstructor
public class ApplicationConfig {
    private final AccountServices accountServices;

    /**
     * User Details Service
     * @return
     */
    @Bean
    public UserDetailsService userDetailsService() {
        return accountServices;
    }

    /**
     * Authentication Manager
     * @param configuration
     * @return
     * @throws Exception
     */
    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration configuration) throws Exception {
        return configuration.getAuthenticationManager();
    }

    /**
     * Authentication Provider
     * @return
     */
    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService());
        authenticationProvider.setPasswordEncoder(passwordEndcoder());
        return authenticationProvider;
    }

    /**
     * Password Encoder
     * @return
     */
    @Bean
    public PasswordEncoder passwordEndcoder() {
        return new BCryptPasswordEncoder();
    }
}
