package com.hieupv.repository;

import com.hieupv.dto.api.results.CreateUserInfoManagerDTO;
import com.hieupv.dto.api.results.UpdateUserInfoManagerDTO;
import com.hieupv.entity.AccountInfoManager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * User Info Manager Repository
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 04-08-2023
 */

@Repository
public interface UserInfoManagerRepository extends JpaRepository<AccountInfoManager, Integer> {
    /**
     * Create User Info Manager
     * @param createUserInfoManagerDTO
     * @throws Exception
     */
    @Modifying
    @Query(value = "INSERT INTO account_info_manager (address" +
                                                      ", avatar_url" +
                                                      ", birthday" +
                                                      ", create_date" +
                                                      ", firstname" +
                                                      ", lastname" +
                                                      ", modify_by_user_id" +
                                                      ", modify_date" +
                                                      ", password" +
                                                      ", phone_number" +
                                                      ", role" +
                                                      ", username) " +
            "VALUES (:#{#createUserInfoManagerDTO.address}" +
                    ", :#{#createUserInfoManagerDTO.avatarUrl}" +
                    ", :#{#createUserInfoManagerDTO.birthday}" +
                    ", CURRENT_TIMESTAMP" +
                    ", :#{#createUserInfoManagerDTO.firstname}" +
                    ", :#{#createUserInfoManagerDTO.lastname}" +
                    ", :#{#createUserInfoManagerDTO.modifyByUserId}" +
                    ", CURRENT_TIMESTAMP " +
                    ", :#{#createUserInfoManagerDTO.password}" +
                    ", :#{#createUserInfoManagerDTO.phoneNumber}" +
                    ", :#{#createUserInfoManagerDTO.role}" +
                    ", :#{#createUserInfoManagerDTO.username})",
            nativeQuery = true)
    @Transactional
    public void createUserInfoManager(CreateUserInfoManagerDTO createUserInfoManagerDTO) throws Exception;

    /**
     * Find By Username
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    public Optional<AccountInfoManager> findByUsername(String username) throws UsernameNotFoundException;

    /**
     * Modify User Info Manager
     * @param updateUserInfoManagerDTO
     */
    @Modifying
    @Transactional
    @Query(value = "UPDATE account_info_manager " +
            "SET username = :#{#updateUserInfoManagerDTO.username}, " +
                "password = :#{#updateUserInfoManagerDTO.password}, " +
                "firstname = :#{#updateUserInfoManagerDTO.firstname}, " +
                "lastname = :#{#updateUserInfoManagerDTO.lastname}, " +
                "birthday = :#{#updateUserInfoManagerDTO.birthday}, " +
                "avatar_url = :#{#updateUserInfoManagerDTO.avatarUrl}, " +
                "phone_number = :#{#updateUserInfoManagerDTO.phoneNumber}, " +
                "address = :#{#updateUserInfoManagerDTO.address}, " +
                "modify_date = CURRENT_TIMESTAMP, " +
                "modify_by_user_id = :#{#updateUserInfoManagerDTO.modifyByUserId}, " +
                "role = :#{#updateUserInfoManagerDTO.role} " +
                "WHERE id = :#{#updateUserInfoManagerDTO.id}" ,
            nativeQuery = true)
    public void modifyUserInfoManager(UpdateUserInfoManagerDTO updateUserInfoManagerDTO) throws Exception;

    /**
     * Delete User By Id
     * @param id
     * @return
     */
    @Transactional
    @Modifying
    @Query(value = "DELETE FROM account_info_manager WHERE id = :id"
            , nativeQuery = true)
    public int deleteUserById (Integer id);

    /**
     * Find By Phone Number
     * @param phoneNumber
     * @return
     * @throws Exception
     */
    @Transactional
    @Query(value = "select * from account_info_manager " +
                   "where phone_number = :phoneNumber ",
            nativeQuery = true)
    public Optional<AccountInfoManager> findByPhoneNumber(String phoneNumber) throws Exception;
}
