package com.hieupv.repository;

import com.hieupv.dto.api.results.CreateCartManagerDTO;
import com.hieupv.dto.api.results.UpdateCartManagerDTO;
import com.hieupv.entity.CartManagement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Cart Manager Repository
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 18-08-2023
 */

@Repository
public interface CartManagerRepository extends JpaRepository<CartManagement, Integer> {
    /**
     * Create Cart Manager
     *
     * @param createCartManagerDTO
     * @throws Exception
     */
    @Modifying
    @Query(value = "INSERT INTO cart_management (total_price" +
            ", total_count" +
            ", quantity_product" +
            ", account_id) " +
            "VALUES (:#{#createCartManagerDTO.totalPrice}" +
            ", :#{#createCartManagerDTO.totalCount}"+
            ", :#{#createCartManagerDTO.quantityProduct}" +
            ", :#{#createCartManagerDTO.accountId})",
            nativeQuery = true)
    @Transactional
    public void createCartManager(CreateCartManagerDTO createCartManagerDTO) throws Exception;

    /**
     * Modify Cart Manager
     *
     * @param updateCartManagerDTO
     */
    @Modifying
    @Transactional
    @Query(value = "UPDATE cart_management " +
            "SET total_price = :#{#updateCartManagerDTO.totalPrice}, " +
            "total_count = :#{#updateCartManagerDTO.totalCount}, " +
            "quantity_product = :#{#updateCartManagerDTO.quantityProduct} ," +
            "WHERE id = :#{#updateCartManagerDTO.id}",
            nativeQuery = true)
    public void modifyCartManager(UpdateCartManagerDTO updateCartManagerDTO) throws Exception;

    /**
     * Delete Cart Management Id
     *
     * @param id
     * @return
     */
    @Transactional
    @Modifying
    @Query(value = "DELETE FROM cart_management WHERE id = :id"
            , nativeQuery = true)
    public int deleteCartManagementById(Integer id) throws Exception;
}

