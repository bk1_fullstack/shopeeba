package com.hieupv.repository;

import com.hieupv.dto.api.results.CreateCategoryManagerDTO;
import com.hieupv.dto.api.results.CreateUserInfoManagerDTO;
import com.hieupv.dto.api.results.UpdateCategoryManagerDTO;
import com.hieupv.dto.api.results.UpdateUserInfoManagerDTO;
import com.hieupv.entity.AccountInfoManager;
import com.hieupv.entity.CategoryManagement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * Category Manager Repository
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 04-08-2023
 */

@Repository
public interface CategoryManagerRepository extends JpaRepository<CategoryManagement, Integer> {
    /**
     * Create Category Manager
     * @param createCategoryManagerDTO
     * @throws Exception
     */
    @Modifying
    @Query(value = "INSERT INTO category_management (category_name" +
                                                      ", create_date" +
                                                      ", modify_by_user_id" +
                                                      ", modify_date) " +
            "VALUES (:#{#createCategoryManagerDTO.categoryName}" +
                    ", CURRENT_TIMESTAMP" +
                    ", :#{#createCategoryManagerDTO.modifyByUserId}" +
                    ", CURRENT_TIMESTAMP)",
            nativeQuery = true)
    @Transactional
    public void createCategoryManager(CreateCategoryManagerDTO createCategoryManagerDTO) throws Exception;

    /**
     * Modify Category Manager
     * @param updateCategoryManagerDTO
     */
    @Modifying
    @Transactional
    @Query(value = "UPDATE category_management " +
            "SET category_name = :#{#updateCategoryManagerDTO.categoryName}, " +
                "modify_by_user_id = :#{#updateCategoryManagerDTO.modifyByUserId}, " +
                "modify_date = CURRENT_TIMESTAMP ," +
                "WHERE id = :#{#updateCategoryManagerDTO.id}" ,
            nativeQuery = true)
    public void modifyCategoryManager(UpdateCategoryManagerDTO updateCategoryManagerDTO) throws Exception;

    /**
     * Delete Category Management Id
     * @param id
     * @return
     */
    @Transactional
    @Modifying
    @Query(value = "DELETE FROM category_management WHERE id = :id"
            , nativeQuery = true)
    public int deleteCategoryManagementById (Integer id) throws Exception;
}

