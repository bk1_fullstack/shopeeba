package com.hieupv.repository;

import com.hieupv.dto.api.results.CreateCategoryManagerDTO;
import com.hieupv.dto.api.results.CreateProductManagerDTO;
import com.hieupv.dto.api.results.UpdateCategoryManagerDTO;
import com.hieupv.dto.api.results.UpdateProductManagerDTO;
import com.hieupv.entity.CategoryManagement;
import com.hieupv.entity.ProductManagement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Product Manager Repository
 * Hieupv
 * Email : hieu.th998@gmail.com
 * Date : 15-08-2023
 */

@Repository
public interface ProductManagerRepository extends JpaRepository<ProductManagement, Integer> {
    @Modifying
    @Query(value = "INSERT INTO product_management (create_date" +
            ", description" +
            ", modify_by_user_id" +
            ", modify_date " +
            ", price " +
            ", product_name " +
            ", quantity_sold " +
            ", stock " +
            ", category_id) " +
            "VALUES (CURRENT_TIMESTAMP" +
            ", :#{#createProductManagerDTO.description}" +
            ", :#{#createProductManagerDTO.modifyByUserId}" +
            ", CURRENT_TIMESTAMP" +
            ", :#{#createProductManagerDTO.price}" +
            ", :#{#createProductManagerDTO.productName}" +
            ", 0" +
            ", :#{#createProductManagerDTO.stock}" +
            ", :#{#createProductManagerDTO.categoryId})",
            nativeQuery = true)
    @Transactional
    public void createProductManager(@Param("createProductManagerDTO") CreateProductManagerDTO createProductManagerDTO) throws Exception;

    /**
     * Modify Product Manager
     * @param updateProductManagerDTO
     * @throws Exception
     */
    @Modifying
    @Transactional
    @Query(value = "UPDATE product_management " +
            "SET description = :#{#updateProductManagerDTO.description}, " +
            "modify_by_user_id = :#{#updateProductManagerDTO.modifyByUserId}, " +
            "modify_date = CURRENT_TIMESTAMP, " +
            "price = :#{#updateProductManagerDTO.price}, " +
            "product_name = :#{#updateProductManagerDTO.productName}, " +
            "stock = :#{#updateProductManagerDTO.stock}, " +
            "category_id = :#{#updateProductManagerDTO.categoryId} " +
            "WHERE id = :#{#updateProductManagerDTO.id}",
            nativeQuery = true)
    public void modifyProductManager(@Param("updateProductManagerDTO") UpdateProductManagerDTO updateProductManagerDTO) throws Exception;

    /**
     * Delete Product Management Id
     * @param id
     * @return
     */
    @Transactional
    @Modifying
    @Query(value = "DELETE FROM product_management WHERE id = :id"
            , nativeQuery = true)
    public int deleteProductManagementById (Integer id) throws Exception;
}

